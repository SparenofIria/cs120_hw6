#include "CTree.h"

using std::string;
using std::cout;
using std::endl;

CTree::CTree(char ch){//creates root node
  data = ch;
  kids = NULL;
  sibs = NULL;
  prev = NULL;
}

CTree::CTree(CTree& old){
  data = old.data;
  *prev = *old.prev;//points to previous
  *sibs = *old.sibs;//points to next sibling
  *kids = *old.kids;//points to next kid
}

CTree::~CTree(){//purpose is to recursively remove all sibs and kids
  if(kids){delete kids;}//only reference one and the destructor will recurse
  if(sibs){delete sibs;}//only reference one and the destructor will recurse
  prev = NULL;
}

// siblings and children must be unique, return true if added, false otherwise
bool CTree::addChild(char ch){
  if(kids){//if there is already a kid...
    return kids->addSibling(ch);//tell existing kid to add a sibling!
  }else{//no other kids, so just add it!
    kids = new CTree(ch);
    kids->prev = this;
  }
  return true;
}

// add tree root for better building, root should have null prev and sibs      
// returns false on any type of failure, including invalid root                
bool CTree::addChild(CTree *root){
  if(!root){
    cout << "Note: Invalid Root" << endl;
    return false;
  }
  if(root->prev || root->sibs){
    cout << "Note: Trying to Add A Non-Root Node to Tree" << endl;
    return false;
  }
  if(kids){
    return kids->addSibling(root);
  }else{//no other kids
    root->prev = this;
    this->kids = root;
  }
  return true;
}

string CTree::toString(){//recursively gets everything
  string retstr = "";//You cannot construct a string with a char
  retstr = retstr + data + "\n";
  if(kids){retstr = retstr + kids->toString();}
  if(sibs){retstr = retstr + sibs->toString();}
  return retstr;
}

// these should only be called from addChild, and have the same restrictions   
// the root of a tree should never have any siblings                           
// returns false on any type of failure, including invalid root                
bool CTree::addSibling(char ch){
  //cout << "Debug: Running addSibling(char ch)" << endl;
  char kiddata = data;//data of the current kid

  if(!prev){//cannot add sibling to root!
    cout << "Note: Trying to Add Sibling To Root Node" << endl;
    return false;
  }

  if(kiddata == ch){
    return false;
  }else if(kiddata<ch){//if new node should be to the right
    if(!sibs){//No sibs
      sibs = new CTree(ch);
      sibs->prev = this;
    }else{
      return sibs->addSibling(ch);//tell the sib to deal with it!
    }
  }else{//if new node should be to the left
    CTree* newtree = new CTree(ch);
    newtree->prev = prev;//Tell New Tree to Point to Old Prev
    newtree->sibs = this;//Tell New Tree to Point to Current Node
    if(prev->kids == this){//if this is the kid of prev
      prev->kids = newtree;//Tell Prev to Point to New Tree
    }else{//prev->sibs == this
      prev->sibs = newtree;//Tell Prev to Point to New Tree
    }
    prev = newtree;//Set Prev to Point to New Tree
  }
  return true;
}

bool CTree::addSibling(CTree *root){
  //cout << "Debug: Running addSibling(CTree *root)" << endl;
  char kiddata = data;//data of the current kid

  if(!prev){//cannot add sibling to root!
    cout << "Note: Trying to Add Sibling To Root Node" << endl;
    return false;
  }

  //cout << "kiddata, root->data: " << kiddata << " " << root->data << endl;  
  if(kiddata == root->data){
    return false;
  }else if(kiddata<root->data){//if new node should be to the right
    if(!sibs){//No sibs
      sibs = root;
      sibs->prev = this;
    }else{
      return sibs->addSibling(root);//tell the sib to deal with it!
    }
  }else{//if new node should be to the left
    root->prev = prev;//Tell New Tree to Point to Old Prev  
    root->sibs = this;//Tell New Tree to Point to Current Node 
    if(prev->kids == this){//if this is the kid of prev               
      prev->kids = root;//Tell Prev to Point to New Tree                    
    }else{//prev->sibs == this                                                 
      prev->sibs = root;//Tell Prev to Point to New Tree                    
    }
    prev = root;//Set Prev to Point to New Tree 
  }
  return true;
}
