#include <string>
#include <sstream>
#include <iostream>
#include <algorithm>
#include "FileDir.h"

using std::string;
using std::endl;

FileDir::FileDir(std::string sysname, long syssize, bool isdirectory){
  name = sysname;
  size = syssize;
  isDirectory = isdirectory;
}

FileDir::FileDir(std::string sysname, long syssize){
  name = sysname;
  size = syssize;
  isDirectory = false;
}

FileDir::FileDir(std::string sysname){
  name = sysname;
  size = 4;
  isDirectory = false;
}

FileDir::FileDir(){
  name = "";
  size = 4;
  isDirectory = false;
}

FileDir::FileDir(const FileDir& other){
  size = other.size;
  name = other.name;
  isDirectory = other.isDirectory;
}

FileDir::~FileDir(){

}

long FileDir::getSize() const{
  return size;
}
string FileDir::getName() const{
  return name;
}
bool FileDir::isFile() const{
  return !isDirectory;
}

//renames a string and returns old name
string FileDir::rename(string newname){
  string oldname = name;
  name = newname;
  return oldname;
}

//adds numinsertions to size if valid
long FileDir::resize(long numinsertions){
  long newsize = size + numinsertions;
  if(newsize>=0){
    size = newsize;
    return size;
  }else{
    return size;
  }
}

string FileDir::toString() const{
  std::stringstream newstring;
  newstring << name;
  if(isDirectory){newstring << "/";}
  newstring << " [" << size << "kb]";
  return newstring.str();
}

bool FileDir::operator ==(const FileDir& rhs){
  if(toString()==rhs.toString() && (isFile()==rhs.isFile())){
    return true;
  }
  return false;
}

bool FileDir::operator <(const FileDir& rhs){
  if(!isFile()&&rhs.isFile()){//left is directory
    return true;
  }else if(isFile()&&!rhs.isFile()){//right is directory
    return false;
  }else{//both file, or both directory
    string leftdata = toString();
    string rightdata = rhs.toString();
    std::transform(leftdata.begin(), leftdata.end(), leftdata.begin(), ::tolower);
    std::transform(rightdata.begin(), rightdata.end(), rightdata.begin(), ::tolower);
    return leftdata < rightdata;
  }
  return false;
}

std::ostream& operator <<(std::ostream& out, const FileDir& rhs){
  out << rhs.toString();
  return out;
}
