#ifndef _FILEDIR_H
#define _FILEDIR_H

class FileDir{
 private:
  std::string name;
  long size;
  bool isDirectory;
 public:

  /*****************CON/DESTRUCTORS*****************/

  FileDir(std::string sysname, long syssize, bool isdirectory);
  FileDir(std::string sysname, long syssize);
  FileDir(std::string sysname);
  FileDir();//Default constructor
  FileDir(const FileDir& other);//Copy constructor
  ~FileDir();

  /*****************ACCESSORS*****************/

  long getSize() const;
  std::string getName() const;
  bool isFile() const;

  /*****************MUTATORS*****************/
  
  //renames a string and returns old name
  std::string rename(std::string newname);

  //adds numinsertions to size if valid
  long resize(long numinsertions);

  /*****************OTHER*****************/

  std::string toString() const;

  bool operator ==(const FileDir& rhs);
  bool operator <(const FileDir& rhs);

};

std::ostream& operator <<(std::ostream& out, const FileDir& rhs);

#endif
