#ifndef _TREE_H
#define _TREE_H

#include <cstdlib>
#include <string>
#include <iostream>
#include <sstream>

// tree of characters, can be used to implement a trie

template <class T> class Tree {
  friend class TreeTest;
  T data;// the value stored in the tree node
  Tree * kids;
  // children - pointer to first child of list, maintain order & uniqueness
  Tree * sibs;  
  // siblings - pointer to rest of children list, maintain order & uniqueness
  // this should always be null if the object is the root of a tree
  Tree * prev;  
  // pointer to parent if this is a first child, or left sibling otherwise
  // this should always be null if the object is the root of a tree
  
 public:
  Tree(T ch){
    data = ch;
    kids = NULL;
    sibs = NULL;
    prev = NULL;
  }
  
  Tree(Tree& old){
    data = old.data;
    *prev = *old.prev;//points to previous
    *sibs = *old.sibs;//points to next sibling
    *kids = *old.kids;//points to next kid   
  }
  
  ~Tree(){  // clear siblings to right and children and this node
    if(kids){delete kids;}//only reference one and the destructor will recurse
    if(sibs){delete sibs;}//only reference one and the destructor will recurse 
    prev = NULL;
  }

  // siblings and children must be unique, return true if added, false otherwise
  bool addChild(T ch){
    if(kids){//if there is already a kid... 
      return kids->addSibling(ch);//tell existing kid to add a sibling!  
    }else{//no other kids, so just add it!                                     
      kids = new Tree(ch);
      kids->prev = this;
    }
    return true;
  }

  // add tree root for better building, root should have null prev and sibs 
  // returns false on any type of failure, including invalid root
  bool addChild(Tree *root){
    if(!root){
      std::cout << "Note: Invalid Root" << std::endl;
      return false;
    }
    if(root->prev || root->sibs){
      std::cout << "Note: Trying to Add A Non-Root Node to Tree" << std::endl;
      return false;
    }
    if(kids){
      return kids->addSibling(root);
    }else{//no other kids
      root->prev = this;
      this->kids = root;
    }
    return true;
  }
  
  std::string toString(){ // all characters, separated by newlines, including at the end
    std::ostringstream oss;
    std::string retstr = "";//You cannot construct a string with a char
    oss << data << std::endl;
    retstr = oss.str();
    if(kids){retstr += kids->toString();}
    if(sibs){retstr += sibs->toString();}
    return retstr;
  }
private:
  // these should only be called from addChild, and have the same restrictions
  // the root of a tree should never have any siblings
  // returns false on any type of failure, including invalid root
  bool addSibling(T ch){
    //cout << "Debug: Running addSibling(char ch)" << endl;                    
    T kiddata = data;//data of the current kid                              
    
    if(!prev){//cannot add sibling to root!                                    
      std::cout << "Note: Trying to Add Sibling To Root Node" << std::endl;
      return false;
    }
    if(kiddata == ch){
      return false;
    }else if(kiddata<ch){//if new node should be to the right                  
      if(!sibs){//No sibs                                                      
	sibs = new Tree(ch);
	sibs->prev = this;
      }else{
	return sibs->addSibling(ch);//tell the sib to deal with it!            
      }
    }else{//if new node should be to the left                                  
      Tree* newtree = new Tree(ch);
      newtree->prev = prev;//Tell New Tree to Point to Old Prev                
      newtree->sibs = this;//Tell New Tree to Point to Current Node            
      if(prev->kids == this){//if this is the kid of prev                      
	prev->kids = newtree;//Tell Prev to Point to New Tree                  
      }else{//prev->sibs == this                                               
	prev->sibs = newtree;//Tell Prev to Point to New Tree       
      }
      prev = newtree;//Set Prev to Point to New Tree     
    }
    return true;
  }
  
  bool addSibling(Tree *root){
    //cout << "Debug: Running addSibling(Tree *root)" << endl;                
    T kiddata = data;//data of the current kid                         
    
    if(!prev){//cannot add sibling to root!  
      std::cout << "Note: Trying to Add Sibling To Root Node" << std::endl;
      return false;
    }
    
    //cout << "kiddata, root->data: " << kiddata << " " << root->data << endl; 
    if(kiddata == root->data){
      return false;
    }else if(kiddata<root->data){//if new node should be to the right          
      if(!sibs){//No sibs                                                      
	sibs = root;
	sibs->prev = this;
      }else{
	return sibs->addSibling(root);//tell the sib to deal with it!          
      }
    }else{//if new node should be to the left                                  
      root->prev = prev;//Tell New Tree to Point to Old Prev                   
      root->sibs = this;//Tell New Tree to Point to Current Node               
      if(prev->kids == this){//if this is the kid of prev                      
	prev->kids = root;//Tell Prev to Point to New Tree                     
      }else{//prev->sibs == this                                               
	prev->sibs = root;//Tell Prev to Point to New Tree                     
      }
      prev = root;//Set Prev to Point to New Tree                             
    }
    return true;
  }
  
} ;

#endif
