#include "Tree.hpp"
#include "FileDir.h"
#include <string>
#include <cassert>
#include <iostream>

using std::cout;
using std::endl;
using std::string;

class TreeTest {
public:
  /* toString and destructor tested throughout (run valgrind to test
   * destructor): "A toString() function that creates and returns a
   * string of all the elements in the nodes of the tree, separated by
   * newline characters. The ordering of the nodes must be determined by
   *
   * a depth-first pre-order traversal from the root node" "A destructor
   * that clears all the children of the current node and all the
   * siblings to the right of the current node, freeing that dynamically
   * allocated memory. (Hint: use recursion!)"
   */


  /* Constructor:
   * "A constructor that is passed the character to store in the root
   * node of a new tree. 
   */
  static void constructorTest() {
    // build a few trees with constructor
    cout << "Test: Constructor Test" << endl;
    Tree<char> t1('A');
    assert(t1.toString() == "A\n");
    Tree<char> t2('b');
    assert(t2.toString() == "b\n");
    Tree<char> t3('^');
    assert(t3.toString() == "^\n");

    //TEST WITH FILEDIR BELOW

    Tree<int> t4(12);
    assert(t4.toString() == "12\n");
    Tree<string> t5("Segmentation");
    assert(t5.toString() == "Segmentation\n");
    //Tree<FileDir> t6('^');
    //assert(t6.toString() == "^\n");
  }


  /* Test adding children and siblings:
   * "An 'addChild' function, which is passed the character data to be
   * stored in the child. This function returns false if it fails
   * (because the data is already a child of this tree node) and true if
   * it succeeds. Remember that it must keep the children ordered."
   *
   * "An 'addSibling' function, which is passed the data to be stored in
   * a new sibling node. This function returns false if it fails
   * (because the data is already a sibling t * o the right of this tree
   * node) and true if it succeeds. Remember that siblings must always
   * be ordered (the hard part of the function, worth 5 points). (This
   * function is really just a helper function for addChild, but keeping
   * it public will let us test it easily.)"
   */
  static void addsTest() {
    cout << "Test: Adding Children and Siblings - Part 1" << endl;
    // A
    Tree<char>* t1Ptr = new Tree<char>('A');
    Tree<char>& t1 = *t1Ptr; // avoids a bunch of *'s below
    assert(t1.toString() == "A\n");

    cout << "Test: Adding Children and Siblings - Part 1A" << endl;

    // A
    // |
    // b
    assert(t1.addChild('b'));
    assert(t1.toString() == "A\nb\n");
    // can't add again
    assert(!t1.addChild('b'));
    assert(t1.toString() == "A\nb\n");

    cout << "Test: Adding Children and Siblings - Part 2" << endl;

    // A
    // |
    // b - c
    assert(t1.addChild('c'));
    assert(t1.toString() == "A\nb\nc\n");

    cout << "Test: Adding Children and Siblings - Part 2A" << endl;
    // can't add again
    assert(!t1.addChild('c'));
    assert(t1.toString() == "A\nb\nc\n");

    cout << "Test: Adding Children and Siblings - Part 3" << endl;
    // A
    // |
    // B - b - c
    assert(t1.addChild('B'));
    // 'B' comes before 'b'
    //cout << "Debug: ToString = \"" << t1.toString() << "\"" << endl;
    assert(t1.toString() == "A\nB\nb\nc\n");
    // can't add repeats
    assert(!t1.addChild('B'));
    assert(!t1.addChild('b'));
    assert(!t1.addChild('c'));
    assert(t1.toString() == "A\nB\nb\nc\n");

    cout << "Test: Adding Children and Siblings - Part 4" << endl;    

    // can't add 'A' as sibling of 'A'
    assert(!t1.addSibling('A'));
    assert(t1.toString() == "A\nB\nb\nc\n");

    // make sure that we can't add siblings to the root
    assert(!t1.addSibling('C'));
    assert(t1.toString() == "A\nB\nb\nc\n");

    cout << "Test: Adding Children and Siblings - Part 5" << endl;

    // Adding in an already built subTree
    // First build another tree
    // R
    Tree<char>* t2Ptr = new Tree<char>('R');
    Tree<char>& t2 = *t2Ptr;
    assert(t2.toString() == "R\n");
        
    // R
    // |
    // C
    assert(t2.addChild('C'));
    assert(t2.toString() == "R\nC\n");


    // R
    // |
    // C - d
    assert(t2.addChild('d'));
    assert(t2.toString() == "R\nC\nd\n");
    // can't repeat
    assert(!t2.addChild('d'));
    assert(t2.toString() == "R\nC\nd\n");

    cout << "Test: Adding Children and Siblings - Part 6" << endl;

    // R
    // |
    // B - C - d
    assert(t2.addChild('B'));
    assert(t2.toString() == "R\nB\nC\nd\n");
    // can't repeat
    assert(!t2.addChild('B'));
    assert(t2.toString() == "R\nB\nC\nd\n");

    // Add t1 in as a child
    // R
    // |
    // A - B - C - d
    // |
    // B - b - c

    // t1 is as before
    assert(t1.toString() == "A\nB\nb\nc\n");

    cout << "Test: Adding Children and Siblings - Part 7" << endl;

    // add t1 to t2
    assert(t2.addChild(&t1));
    // t1 should now have siblings
    assert(t1.toString() == "A\nB\nb\nc\nB\nC\nd\n");
    // t2 should be updated
    assert(t2.toString() == "R\nA\nB\nb\nc\nB\nC\nd\n");

    cout << "Test: Adding Children and Siblings - Part 8" << endl;

    // R
    // |
    // @ - A - B - C - d
    //     |
    //     B - b - c
    assert(t2.addChild('@'));
    assert(t2.toString() == "R\n@\nA\nB\nb\nc\nB\nC\nd\n");
    // shouldn't be able to add duplicate children
    assert(!t2.addChild('@'));
    assert(!t2.addChild('A'));
    assert(!t2.addChild('B'));
    assert(!t2.addChild('C'));
    assert(!t2.addChild('d'));

    cout << "Test: Adding Children and Siblings - Part 9" << endl;
    // R
    // |
    // @ - A - B - C - D - d
    //     |
    //     B - b - c
    assert(t2.addChild('D'));
    //cout << "Debug: ToString = \"" << t2.toString() << "\"" << endl;
    assert(t2.toString() == "R\n@\nA\nB\nb\nc\nB\nC\nD\nd\n");

    // R
    // |
    // @ - A - B - C - D - d - e
    //     |
    //     B - b - c
    assert(t2.addChild('e'));
    assert(t2.toString() == "R\n@\nA\nB\nb\nc\nB\nC\nD\nd\ne\n");

    cout << "Test: Adding Children and Siblings - Part 10" << endl;

    delete t2Ptr;

    //INT
    cout << "Test: Adding Children and Siblings - Part 11" << endl;
    // 1                                                                   
    Tree<int>* t3Ptr = new Tree<int>(1);
    Tree<int>& t3 = *t3Ptr; // avoids a bunch of *'s below             
    assert(t3.toString() == "1\n");

    // 1                                                                        
    // |                                                                      
    // 7                                                                       
    assert(t3.addChild(7));
    assert(t3.toString() == "1\n7\n");
    // can't add again                                                          
    assert(!t3.addChild(7));
    assert(t3.toString() == "1\n7\n");

    // 1                                                                       
    // |                    
    // 7 - 8                                                                    
    assert(t3.addChild(8));
    assert(t3.toString() == "1\n7\n8\n");
    // can't add again                                                          
    assert(!t3.addChild(8));
    assert(t3.toString() == "1\n7\n8\n");

    // 1                                                                        
    // |                                                                        
    // 2 - 7 - 8                                           
    assert(t3.addChild(2));
    // '2' comes before '7'                                                     
    //cout << "Debug: ToString = \"" << t3.toString() << "\"" << endl;          
    assert(t3.toString() == "1\n2\n7\n8\n");
    // can't add repeats                                                        
    assert(!t3.addChild(2));
    assert(!t3.addChild(7));
    assert(!t3.addChild(8));
    assert(t3.toString() == "1\n2\n7\n8\n");

    // can't add '1' as sibling of '1'                                          
    assert(!t3.addSibling(1));
    assert(t3.toString() == "1\n2\n7\n8\n");

    // make sure that we can't add siblings to the root                         
    assert(!t3.addSibling(3));
    assert(t3.toString() == "1\n2\n7\n8\n");

    // Adding in an already built subTree                                       
    // First build another tree                                                 
    // 5                                                                        
    Tree<int>* t4Ptr = new Tree<int>(5);
    Tree<int>& t4 = *t4Ptr;
    assert(t4.toString() == "5\n");

    // 5                                                                        
    // |                                                                        
    // 3                                                                        
    assert(t4.addChild(3));
    assert(t4.toString() == "5\n3\n");

    // 5                                                                        
    // |                                                                        
    // 3 - 9                                                                    
    assert(t4.addChild(9));
    assert(t4.toString() == "5\n3\n9\n");
    // can't repeat                                                             
    assert(!t4.addChild(9));
    assert(t4.toString() == "5\n3\n9\n");

    // 5                                                                        
    // |                                                                        
    // 2 - 3 - 9                                                                
    assert(t4.addChild(2));
    assert(t4.toString() == "5\n2\n3\n9\n");
    // can't repeat                                                             
    assert(!t4.addChild(2));
    assert(t4.toString() == "5\n2\n3\n9\n");

    // Add t3 in as a child                                                     
    // 5                                                                        
    // |                                                                        
    // 1 - 2 - 3 - 9                                                            
    // |                                                                        
    // 2 - 7 - 8                                                                

    // t3 is as before                                                          
    assert(t3.toString() == "1\n2\n7\n8\n");

    cout << "Test: Adding Children and Siblings - Part 12" << endl;

    // add t3 to t4                                                             
    assert(t4.addChild(&t3));
    // t3 should now have siblings                                              
    assert(t3.toString() == "1\n2\n7\n8\n2\n3\n9\n");
    // t4 should be updated                                                     
    assert(t4.toString() == "5\n1\n2\n7\n8\n2\n3\n9\n");

    // 5                                                                        
    // |                                                                        
    // 0 - 1 - 2 - 3 - 9                                                        
    //     |                                                                    
    //     2 - 7 - 8                                                            
    assert(t4.addChild(0));
    assert(t4.toString() == "5\n0\n1\n2\n7\n8\n2\n3\n9\n");
    // shouldn't be able to add duplicate children                              
    assert(!t4.addChild(0));
    assert(!t4.addChild(1));
    assert(!t4.addChild(2));
    assert(!t4.addChild(3));
    assert(!t4.addChild(9));

    // 5                                                                        
    // |                                                                        
    // 0 - 1 - 2 - 3 - 4 - 9                                                    
    //     |                                                                    
    //     2 - 7 - 8                                                            
    assert(t4.addChild(4));
    //cout << "Debug: ToString = \"" << t4.toString() << "\"" << endl;          
    assert(t4.toString() == "5\n0\n1\n2\n7\n8\n2\n3\n4\n9\n");

    delete t4Ptr;
     
    cout << "Test: Adding Children and Siblings - All Char and Int Tests Cleared" << endl;
  }

  static void addsTestString(){
    cout << "Test: Adding Children and Siblings - Part 13" << endl;
    // Air                                                                      
    Tree<string>* t5Ptr = new Tree<string>("Air");
    Tree<string>& t5 = *t5Ptr; // avoids Air bunch of *'s below                 
    assert(t5.toString() == "Air\n");

    // Air                                                                      
    // |                                                                        
    // b                                                                        
    assert(t5.addChild("b"));
    assert(t5.toString() == "Air\nb\n");
    // can't add again                                                          
    assert(!t5.addChild("b"));
    assert(t5.toString() == "Air\nb\n");

    // Air                                                                      
    // |                                                                        
    // b - c                                                                    
    assert(t5.addChild("c"));
    assert(t5.toString() == "Air\nb\nc\n");
    // can"t add again                                                          
    assert(!t5.addChild("c"));
    assert(t5.toString() == "Air\nb\nc\n");

    // Air                                                                      
    // |                                                                        
    // B - b - c                                           
    assert(t5.addChild("B"));
    // "B" comes before "b"                                                     
    //cout << "Debug: ToString = \"" << t5.toString() << "\"" << endl;          
    assert(t5.toString() == "Air\nB\nb\nc\n");
    // can"t add repeats                                                        
    assert(!t5.addChild("B"));
    assert(!t5.addChild("b"));
    assert(!t5.addChild("c"));
    assert(t5.toString() == "Air\nB\nb\nc\n");

    // can"t add "Air" as sibling of "Air"                                      
    assert(!t5.addSibling("Air"));
    assert(t5.toString() == "Air\nB\nb\nc\n");

    // make sure that we can"t add siblings to the root                         
    assert(!t5.addSibling("C"));
    assert(t5.toString() == "Air\nB\nb\nc\n");

    // Adding in an already built subTree                                       
    // First build another tree                                                 
    // Ryu

    Tree<string>* t6Ptr = new Tree<string>("Ryu");
    Tree<string>& t6 = *t6Ptr;
    assert(t6.toString() == "Ryu\n");

    // Ryu                                                                      
    // |                                                                        
    // C                                                                        
    assert(t6.addChild("C"));
    assert(t6.toString() == "Ryu\nC\n");

    // Ryu                                                                      
    // |                                                                        
    // C - dyne                                                                 
    assert(t6.addChild("dyne"));
    assert(t6.toString() == "Ryu\nC\ndyne\n");
    // can"t repeat                                                             
    assert(!t6.addChild("dyne"));
    assert(t6.toString() == "Ryu\nC\ndyne\n");

    // Ryu                                                                      
    // |                                                                        
    // B - C - dyne                                                             
    assert(t6.addChild("B"));
    assert(t6.toString() == "Ryu\nB\nC\ndyne\n");
    // can"t repeat                                                             
    assert(!t6.addChild("B"));
    assert(t6.toString() == "Ryu\nB\nC\ndyne\n");

    // Add t5 in as Air child                                                   
    // Ryu                                                                      
    // |                                                                        
    // Air - B - C - dyne                                                       
    // |                                                                        
    // B - b - c                                                                

    // t5 is as before                                                          
    assert(t5.toString() == "Air\nB\nb\nc\n");

    cout << "Test: Adding Children and Siblings - Part 14" << endl;

    // add t5 to t6                                                             
    assert(t6.addChild(&t5));
    // t5 should now have siblings                                              
    assert(t5.toString() == "Air\nB\nb\nc\nB\nC\ndyne\n");
    // t6 should be updated                                                     
    assert(t6.toString() == "Ryu\nAir\nB\nb\nc\nB\nC\ndyne\n");

    // Ryu                                                                      
    // |                                                                        
    // @ - Air - B - C - d                                                      
    //     |                                                                    
    //     B - b - c                                                            
    assert(t6.addChild("@"));
    assert(t6.toString() == "Ryu\n@\nAir\nB\nb\nc\nB\nC\ndyne\n");
    // shouldn't be able to add duplicate children                              
    assert(!t6.addChild("@"));
    assert(!t6.addChild("Air"));
    assert(!t6.addChild("B"));
    assert(!t6.addChild("C"));
    assert(!t6.addChild("dyne"));

    // Ryu                                                                      
    // |                                                                        
    // @ - Air - B - C - D - dyne                                               
    //     |                                                                    
    //     B - b - c                                                            
    assert(t6.addChild("D"));
    //cout << "Debug: ToString = \"" << t6.toString() << "\"" << endl;          
    assert(t6.toString() == "Ryu\n@\nAir\nB\nb\nc\nB\nC\nD\ndyne\n");

    // Ryu                                                                      
    // |                                                                        
    // @ - Air - B - C - D - dyne - eae                                         
    //     |                                                                    
    //     B - b - c                                                            
    assert(t6.addChild("eae"));
    assert(t6.toString() == "Ryu\n@\nAir\nB\nb\nc\nB\nC\nD\ndyne\neae\n");

    delete t6Ptr;

    cout << "Test: Adding Children and Siblings - All String Tests Cleared" << endl;
  }

  static void addsTestFileDir(){
    cout << "Test: Adding Children and Siblings - Part 15" << endl;
    // A       
    Tree<FileDir>* t7Ptr = new Tree<FileDir>(FileDir("A", 5, true));
    Tree<FileDir>& t7 = *t7Ptr; // avoids a bunch of *'s below             
    assert(t7.toString() == "A/ [5kb]\n");

    // A       
    // |       
    // b       
    assert(t7.addChild(FileDir("b", 5, false)));
    assert(t7.toString() == "A/ [5kb]\nb [5kb]\n");
    // can't add again            
    assert(!t7.addChild(FileDir("b", 5, false)));
    assert(t7.toString() == "A/ [5kb]\nb [5kb]\n");

    // A       
    // |       
    // b - c   
    assert(t7.addChild(FileDir("c", 5, false)));
    assert(t7.toString() == "A/ [5kb]\nb [5kb]\nc [5kb]\n");
    // can"t add again            
    assert(!t7.addChild(FileDir("c", 5, false)));
    assert(t7.toString() == "A/ [5kb]\nb [5kb]\nc [5kb]\n");

    // A       
    // |       
    // B - b - c     
    assert(t7.addChild(FileDir("B", 5, true)));
    // FileDir(B, 5, true) comes before FileDir(b, 5, false)       
    //cout << "Debug: ToString = \"" << t7.toString() << "\"" << endl;  
    assert(t7.toString() == "A/ [5kb]\nB/ [5kb]\nb [5kb]\nc [5kb]\n");
    // can"t add repeats          
    assert(!t7.addChild(FileDir("B", 5, true)));
    assert(!t7.addChild(FileDir("b", 5, false)));
    assert(!t7.addChild(FileDir("c", 5, false)));
    assert(t7.toString() == "A/ [5kb]\nB/ [5kb]\nb [5kb]\nc [5kb]\n");

    // can"t add FileDir(A, 5, true) as sibling of FileDir(A, 5, true)               
    assert(!t7.addSibling(FileDir("A", 5, true)));
    assert(t7.toString() == "A/ [5kb]\nB/ [5kb]\nb [5kb]\nc [5kb]\n");

    // make sure that we can"t add siblings to the root                 
    assert(!t7.addSibling(FileDir("C", 5, true)));
    assert(t7.toString() == "A/ [5kb]\nB/ [5kb]\nb [5kb]\nc [5kb]\n");

    // Adding in an already built subTree            
    // First build another tree   
    // R       
    Tree<FileDir>* t8Ptr = new Tree<FileDir>(FileDir("R", 5, true));
    Tree<FileDir>& t8 = *t8Ptr;
    assert(t8.toString() == "R/ [5kb]\n");

    // R       
    // |       
    // C       
    assert(t8.addChild(FileDir("C", 5, true)));
    assert(t8.toString() == "R/ [5kb]\nC/ [5kb]\n");

    // R       
    // |       
    // C - d   
    assert(t8.addChild(FileDir("d", 5, false)));
    assert(t8.toString() == "R/ [5kb]\nC/ [5kb]\nd [5kb]\n");
    // can"t repeat               
    assert(!t8.addChild(FileDir("d", 5, false)));
    assert(t8.toString() == "R/ [5kb]\nC/ [5kb]\nd [5kb]\n");

    // R       
    // |       
    // B - C - d                  
    assert(t8.addChild(FileDir("B", 5, true)));
    assert(t8.toString() == "R/ [5kb]\nB/ [5kb]\nC/ [5kb]\nd [5kb]\n");
    // can"t repeat               
    assert(!t8.addChild(FileDir("B", 5, true)));
    assert(t8.toString() == "R/ [5kb]\nB/ [5kb]\nC/ [5kb]\nd [5kb]\n");

    // Add t7 in as a child       
    // R       
    // |       
    // A - B - C - d              
    // |       
    // B - b - c                  

    // t7 is as before            
    assert(t7.toString() == "A/ [5kb]\nB/ [5kb]\nb [5kb]\nc [5kb]\n");

    cout << "Test: Adding Children and Siblings - Part 16" << endl;

    // add t7 to t8               
    assert(t8.addChild(&t7));
    // t7 should now have siblings
    assert(t7.toString() == "A/ [5kb]\nB/ [5kb]\nb [5kb]\nc [5kb]\nB/ [5kb]\nC/ [5kb]\nd [5kb]\n");
    // t8 should be updated       
    assert(t8.toString() == "R/ [5kb]\nA/ [5kb]\nB/ [5kb]\nb [5kb]\nc [5kb]\nB/ [5kb]\nC/ [5kb]\nd [5kb]\n");

    // R       
    // |       
    // @ - A - B - C - d          
    //     |   
    //     B - b - c              
    assert(t8.addChild(FileDir("@", 5, true)));
    assert(t8.toString() == "R/ [5kb]\n@/ [5kb]\nA/ [5kb]\nB/ [5kb]\nb [5kb]\nc [5kb]\nB/ [5kb]\nC/ [5kb]\nd [5kb]\n");
    // shouldn"t be able to add duplicate children   
    assert(!t8.addChild(FileDir("@", 5, true)));
    assert(!t8.addChild(FileDir("A", 5, true)));
    assert(!t8.addChild(FileDir("B", 5, true)));
    assert(!t8.addChild(FileDir("C", 5, true)));
    assert(!t8.addChild(FileDir("d", 5, false)));

    // R       
    // |       
    // @ - A - B - C - D - d      
    //     |   
    //     B - b - c              
    assert(t8.addChild(FileDir("D", 5, true)));
    //cout << "Debug: ToString = \"" << t8.toString() << "\"" << endl;  
    assert(t8.toString() == "R/ [5kb]\n@/ [5kb]\nA/ [5kb]\nB/ [5kb]\nb [5kb]\nc [5kb]\nB/ [5kb]\nC/ [5kb]\nD/ [5kb]\nd [5kb]\n");

    // R       
    // |       
    // @ - A - B - C - D - d - e  
    //     |   
    //     B - b - c              
    assert(t8.addChild(FileDir("e", 5, false)));
    assert(t8.toString() == "R/ [5kb]\n@/ [5kb]\nA/ [5kb]\nB/ [5kb]\nb [5kb]\nc [5kb]\nB/ [5kb]\nC/ [5kb]\nD/ [5kb]\nd [5kb]\ne [5kb]\n");

    delete t8Ptr;

    cout << "Test: Adding Children and Siblings - All FileDir Tests Cleared\
" << endl;
  }

  // adds a single child
  static void addSimpleChildTest() {
    cout << "Test: Adding Simple Child - Part 1" << endl;
    // A
    Tree<char>* t1 = new Tree<char>('A');
    assert(t1->toString() == "A\n");

    // A
    Tree<char>* t2 = new Tree<char>('A');
    assert(t2->toString() == "A\n");

    cout << "Test: Adding Simple Child - Part 2" << endl;

    // A
    // |
    // A
    assert(t2->addChild(t1));
    assert(t2->toString() == "A\nA\n");

    cout << "Test: Adding Simple Child - Part 3" << endl;

    delete t2;

  }

};



int main(void) {
  cout << "Testing Tree" << endl;
  TreeTest::constructorTest();
  TreeTest::addsTest();
  TreeTest::addsTestString();
  TreeTest::addsTestFileDir();
  TreeTest::addSimpleChildTest();
  cout << "Tree tests passed" << endl;
}
